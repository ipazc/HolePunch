cmake_minimum_required(VERSION 3.3)
project(HolePunchTest)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -pthread")

set(SOURCE_FILES main.cpp StandardServer/StandardServer.h StandardServer/Socket.cpp StandardServer/Socket.h StandardServer/Packet.h StandardServer/Host.h Exception/SocketException.h Events/Event.h Events/OnConnectedEvent.h StandardServer/StandardServer.cpp ThreadPool/PoolThread.cpp ThreadPool/PoolThread.h ThreadPool/ThreadPool.cpp ThreadPool/ThreadPool.h ThreadPool/FunctionWrapper.cpp ThreadPool/FunctionWrapper.h)
add_executable(HolePunchTest ${SOURCE_FILES})