//
// Created by ivan on 18/09/15.
//

#ifndef HOLEPUNCHTEST_ONCONNECTEDEVENT_H
#define HOLEPUNCHTEST_ONCONNECTEDEVENT_H

#include "Event.h"

template<class T>
class OnConnectedEvent : public Event<T>
{
private:
    void OnEvent(T pRaiser) override
    {
        OnConnected(pRaiser);
    }

public:
    virtual void OnConnected(T pRaiser);
};

#endif //HOLEPUNCHTEST_ONCONNECTEDEVENT_H
