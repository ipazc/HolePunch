//
// Created by ivan on 17/09/15.
//

#ifndef HOLEPUNCHTEST_EVENT_H
#define HOLEPUNCHTEST_EVENT_H

template<class T>
class Event
{
public:
    virtual void OnEvent(T pRaiser);
};


#endif //HOLEPUNCHTEST_EVENT_H
