//
// Created by ivan on 18/09/15.
//

#ifndef HOLEPUNCHTEST_ONDISCONNECTEDEVENT_H
#define HOLEPUNCHTEST_ONDISCONNECTEDEVENT_H

#include "Event.h"

template<class T>
class OnDisconnectedEvent : public Event<T>
{
private:
    void OnEvent(T pRaiser) override
    {
        OnDisconnected(pRaiser);
    }

public:
    virtual void OnDisconnected(T pRaiser);
};

#endif //HOLEPUNCHTEST_ONDISCONNECTEDEVENT_H
