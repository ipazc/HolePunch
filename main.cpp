#include <iostream>
#include <functional>
#include "StandardServer/Socket.h"

using namespace std;

class test : public OnConnectedEvent<Socket *>, public OnDisconnectedEvent<Socket *>
{
public:
    void OnConnected(Socket *pSocket)
    {
        cout << "¡Conectado a " << pSocket->GetRemoteHost()->GetAddress() << ":" <<
        pSocket->GetRemoteHost()->GetPort() << "!\n";
    }

    void OnDisconnected(Socket *pSocket)
    {
        cout << "¡Desconectado de " << pSocket->GetRemoteHost()->GetAddress() << ":" <<
        pSocket->GetRemoteHost()->GetPort() << "!\n";
    }
};

using namespace std;

int main()
{


    try
    {
        test prueba;

        template<class F>
        function<void(F*, Socket*)> memberFunc = &test::OnConnected;

        void* member = &prueba;
        /*auto Callable = bind (memberFunc, member);*/
        Socket lSocket;
        lSocket.SetOnConnected(&prueba);
        lSocket.SetOnDisconnected(&prueba);

        lSocket.Connect("dedicado3.sockhost.net", 80);

        memberFunc((test*)member, &lSocket);
/*        evento st = &test::OnConnected;
        (prueba.*st)(&lSocket);
*/
        lSocket.Disconnect();
    } catch (SocketException *socketException)
    {
        cout << string(socketException->what()) << "\n";
    }



    cout << "Hello, World! " << endl;
    return 0;
}