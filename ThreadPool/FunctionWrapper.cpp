//
// Created by ivan on 28/09/15.
//

#include "FunctionWrapper.h"

FunctionWrapper::FunctionWrapper(function<void(T *, void *)> pFunctionToCall, T *pMember)
{
    this->functionToCall = pFunctionToCall;
    this->member = pMember;
}

void FunctionWrapper::operator()(void *pParameter)
{
    this->functionToCall(this->member, pParameter);
}
