//
// Created by ivan on 28/09/15.
//

#ifndef HOLEPUNCHTEST_FUNCTIONWRAPPER_H
#define HOLEPUNCHTEST_FUNCTIONWRAPPER_H

#include <thread>

using namespace std;

template<class T>
class FunctionWrapper
{
private:
    function<void(T*, void*)> functionToCall;
    void* member;
public:
    FunctionWrapper(function<void(T*, void*)> pFunctionToCall, T* pMember);
    void operator()(void* pParameter);
};


#endif //HOLEPUNCHTEST_FUNCTIONWRAPPER_H
