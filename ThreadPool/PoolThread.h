//
// Created by ivan on 28/09/15.
//

#ifndef HOLEPUNCHTEST_POOLTHREAD_H
#define HOLEPUNCHTEST_POOLTHREAD_H

#include <thread>
#include "FunctionWrapper.h"

using namespace std;


class PoolThread
{
private:
    function<void(void* pParameter)> threadFunction;
    void* classOwner;

    thread* standardThread;
    void functionLoop();
    int threadResult = -1;
public:
    PoolThread();

    void WrapFunction(FunctionWrapper pFunctionWrapper);
    bool Start(void* pParameter);
    bool Join();
    bool GetStatus();

    ~PoolThread();
};



#endif //HOLEPUNCHTEST_POOLTHREAD_H
