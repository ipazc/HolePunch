//
// Created by ivan on 16/09/15.
//

#ifndef HOLEPUNCHTEST_STANDARDSERVER_H
#define HOLEPUNCHTEST_STANDARDSERVER_H

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <iostream>
#include <list>
#include <thread>
#include <poll.h>
#include <mutex>
#include <atomic>
#include "Host.h"
#include "Socket.h"


#define DEFAULT_PORT 12321
#define MAX_CONNECTIONS_BACKLOG 32

using namespace std;

enum STATUS
{
    LISTENING,
    STOPPED
};

class StandardServer
{
private:
    int socketFileDescriptor = -1;
    list<Host*> bindedHosts;
    atomic<bool> stopListening;

    void disposeHosts();
    void listen();
    void disposeSockets();
protected:
    bool isListening = false;
    STATUS status = STOPPED;

    mutex socketListMutex;
    list<Socket*> socketList;

    list<Socket*> serverSocket;
    thread* serverThread;

public:
    /**
     * Creates a new StandardServer instance and binds the specified IP and port to it.
     * If no arguments are passed, it will fall back to every net interface in the computer
     * and the default port 12321.
     */
    StandardServer(string pHost = "0.0.0.0", int pPort = DEFAULT_PORT);


    void BindHost(string pHost, int pPort);


    list<Host*>::const_iterator BindedHostsIterator();

    void Listen();

    STATUS GetStatus();

    void Stop();

    void RemoveBindHost(Host* pHost);

    ~StandardServer();
};

#endif //HOLEPUNCHTEST_STANDARDSERVER_H
