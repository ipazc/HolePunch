//
// Created by ivan on 16/09/15.
//

#ifndef HOLEPUNCHTEST_PACKET_H
#define HOLEPUNCHTEST_PACKET_H

struct Packet
{
private:
    void* data;
    ssize_t size;
public:
    Packet(void* pData, ssize_t pSize) : data(pData), size(pSize) { };

    void* GetData() { return data; };

    void SetData(void* pData, ssize_t pSize)
    {
        data = pData;
        size = pSize;
    };

    void SetSize(ssize_t pSize)
    {
        size = pSize;
    }

    ssize_t GetSize() { return size; };
};

#endif //HOLEPUNCHTEST_PACKET_H
