//
// Created by ivan on 16/09/15.
//

#ifndef HOLEPUNCHTEST_SOCKET_H
#define HOLEPUNCHTEST_SOCKET_H

#include "Host.h"
#include "Packet.h"
#include "../Events/Event.h"
#include "../Events/OnConnectedEvent.h"
#include "../Events/OnDisconnectedEvent.h"
#include "../Exception/SocketException.h"
#include <iostream>
#include <stdexcept>
#include <string.h>

const int DEFAULT_TIMEOUT_IN_MILLISECONDS = 1000;
const int MAX_ERROR_LEN = 256;


using namespace std;

class Socket
{
private:
    // Every derived class from this must define the "super" instance:
    //typedef Socket super;

    int socketFileDescriptor = -1;

    Event<Socket *> *onConnected = NULL;
    Event<Socket *> *onDisconnected = NULL;

    bool isInitialized();

    void initialize(int pSocketFileDescriptor = -1);

    void finalize();

    void bindLocalhost();

    void extractLocalHostFromDescriptor();

    void extractRemoteHostFromDescriptor();

    static string getLastError();

protected:
    Host *localHost = NULL;
    Host *remoteHost = NULL;

    int timeout = 0;

public:
    Socket(string pLocalHost = "0.0.0.0", int pLocalPort = 0);

    Socket(int pSocketFileDescriptor);

    void SetReuseSocket(bool pSetToReuse = true);

    void SetTimeout(int pTimeout = DEFAULT_TIMEOUT_IN_MILLISECONDS);

    void SetKeepAlive(bool pSetKeepAlive = true);

    virtual bool SendFixedPacket(Packet *pPacket);
    virtual bool ReadFixedPacket(Packet *pPacket);

    Host *GetRemoteHost() { return this->remoteHost; };

    Host *GetLocalHost() { return this->localHost; };

    void Connect(string pHost, int pPort);

    void Disconnect();

    bool IsConnected();

    void SetOnConnected(OnConnectedEvent<Socket *> *pOnConnected) { onConnected = pOnConnected; };

    void SetOnDisconnected(OnDisconnectedEvent<Socket *> *pOnDisconnected) { onDisconnected = pOnDisconnected; };

    int GetDescriptor() { return socketFileDescriptor; };

    bool Listen(int pBackLog);
    ~Socket();
};


#endif //HOLEPUNCHTEST_SOCKET_H
