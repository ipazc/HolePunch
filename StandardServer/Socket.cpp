//
// Created by ivan on 16/09/15.
//


#include "Socket.h"

Socket::Socket(string pLocalHost, int pLocalPort)
{
    this->localHost = new Host(pLocalHost, pLocalPort);
    this->remoteHost = NULL;
    initialize();
}

Socket::Socket(int pSocketFileDescriptor)
{
    if (pSocketFileDescriptor < 0)
    {
        throw new SocketException("A valid socket file descriptor is needed to initialize the socket.");
    }

    initialize(pSocketFileDescriptor);
    extractLocalHostFromDescriptor();
    extractRemoteHostFromDescriptor();
}

void Socket::initialize(int pSocketFileDescriptor)
{
    if (isInitialized())
    {
        return;
    }

    if (pSocketFileDescriptor > -1)
    {
        this->socketFileDescriptor = pSocketFileDescriptor;
    } else
    {
        this->socketFileDescriptor = socket(AF_INET, SOCK_STREAM, 0);
    }

    bindLocalhost();
}

bool Socket::isInitialized()
{
    return this->socketFileDescriptor != -1;
}

string Socket::getLastError()
{
    return string(strerror(errno));
}

void Socket::bindLocalhost()
{
    if (!isInitialized())
    {
        throw new SocketException("Socket must be initialized before binding");
    }

    sockaddr_in socketAddress = localHost->GetNetworkSocketAddress();

    int bindResult = bind(socketFileDescriptor, (sockaddr *) &socketAddress, sizeof(socketAddress));

    if (bindResult == -1)
    {
        throw new SocketException("Error at socket localhost bind: " + getLastError());
    }
}


void Socket::finalize()
{
    if (!isInitialized())
    {
        return;
    }

    shutdown(this->socketFileDescriptor, SHUT_RDWR);

    if (remoteHost)
    {
        delete remoteHost;
        remoteHost = NULL;
    }

    this->socketFileDescriptor = -1;
}


void Socket::extractLocalHostFromDescriptor()
{
    if (this->localHost)
    {
        delete this->localHost;
    }

    char addressString[INET_ADDRSTRLEN];
    sockaddr_in localSocketInformation{};
    socklen_t localSocketSize = sizeof(localSocketInformation);

    getsockname(this->socketFileDescriptor, (sockaddr *) &localSocketInformation,
                &localSocketSize);

    inet_ntop(AF_INET, &(localSocketInformation.sin_addr), addressString, INET_ADDRSTRLEN);

    this->localHost = new Host(string(addressString), ntohs(localSocketInformation.sin_port));
}

void Socket::extractRemoteHostFromDescriptor()
{
    if (this->remoteHost)
    {
        delete this->remoteHost;
    }

    char addressString[INET_ADDRSTRLEN];
    sockaddr_in remoteSocketInformation{};
    socklen_t remoteSocketSize = sizeof(remoteSocketInformation);

    getpeername(this->socketFileDescriptor, (sockaddr *) &remoteSocketInformation,
                &remoteSocketSize);

    inet_ntop(AF_INET, &(remoteSocketInformation.sin_addr), addressString, INET_ADDRSTRLEN);

    this->remoteHost = new Host(string(addressString), ntohs(remoteSocketInformation.sin_port));
}

void Socket::Connect(string pHost, int pPort)
{
    if (IsConnected())
    {
        throw new SocketException("A connected socket can't establish a new connection without a previous disconnect.");
    }

    if (!isInitialized())
    {
        initialize();
    }

    if (this->remoteHost)
    {
        delete this->remoteHost;
    }

    this->remoteHost = new Host(pHost, pPort);

    sockaddr_in remoteHost = this->remoteHost->GetNetworkSocketAddress();

    int connectStatus = connect(this->socketFileDescriptor, (sockaddr *) &remoteHost, sizeof(remoteHost));

    if (connectStatus == -1)
    {
        throw new SocketException("Couldn't establish connection to the remote host: " + getLastError());
    }

    //DONE: Call onConnect event.
    if (onConnected)
    {
        onConnected->OnEvent(this);
    }
}

bool Socket::ReadFixedPacket(Packet *pPacket)
{
    ssize_t lReadBytes = recv(this->socketFileDescriptor, pPacket->GetData(), pPacket->GetSize(), MSG_WAITALL);

    if (lReadBytes == 0)
        Disconnect();

    return lReadBytes == pPacket->GetSize();
}

bool Socket::IsConnected()
{
    return remoteHost != NULL;
}

void Socket::Disconnect()
{
    //DONE: call onDisconnect event when "if (IsConnected())"
    if (IsConnected() && onDisconnected)
    {
        onDisconnected->OnEvent(this);
    }
    finalize();
}

Socket::~Socket()
{
    Disconnect();

    if (this->localHost)
    {
        delete this->localHost;
    }

    if (this->remoteHost)
    {
        delete this->remoteHost;
    }
}

bool Socket::SendFixedPacket(Packet *pPacket)
{
    ssize_t lBytesWritten = send(this->socketFileDescriptor, pPacket->GetData(), pPacket->GetSize(), 0);

    return lBytesWritten == pPacket->GetSize();
}

bool Socket::Listen(int pBackLog)
{
    return listen(this->socketFileDescriptor, pBackLog) >= 0;
}
