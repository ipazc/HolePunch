//
// Created by ivan on 18/09/15.
//

#include "StandardServer.h"

StandardServer::StandardServer(string pHost, int pPort)
{
    stopListening = false;
    this->BindHost(pHost, pPort);
}

void StandardServer::BindHost(string pHost, int pPort)
{
    Host* host = new Host(pHost, pPort);
    this->bindedHosts.push_back(host);
}

list<Host*>::const_iterator StandardServer::BindedHostsIterator()
{
    return this->bindedHosts.begin();
}

void StandardServer::RemoveBindHost(Host* pHost)
{
    this->bindedHosts.remove(pHost);
}

STATUS StandardServer::GetStatus()
{
    return status;
}

StandardServer::~StandardServer()
{
    Stop();
    disposeSockets();
    disposeHosts();
}

void StandardServer::Stop()
{
    if (serverThread)
    {
        stopListening = true;
        serverThread->join();
        serverThread = NULL;
    }

    for (list<Socket*>::const_iterator iterator = serverSocket.begin(); iterator != serverSocket.end(); ++iterator)
    {
        Socket* socket = *iterator;
        delete socket;
    }
    serverSocket.clear();

    status = STOPPED;
}

void StandardServer::Listen()
{
    if (GetStatus() == LISTENING)
        return;

    for (list<Host*>::const_iterator iterator = bindedHosts.begin(); iterator != bindedHosts.end(); ++iterator)
    {
        Host* host = *iterator;
        Socket* listenSocket = new Socket(host->GetAddress(), host->GetPort());

        if (listenSocket->Listen(MAX_CONNECTIONS_BACKLOG))
            serverSocket.push_back(listenSocket);
        else
        {
            //TODO: Log warning: couldn't listen on specified socket
            delete listenSocket;
        }
    }

    stopListening = false;
    serverThread = new std::thread(&StandardServer::listen, this);
    status = LISTENING;
}

void StandardServer::listen()
{
    nfds_t socketsCount = serverSocket.size();
    pollfd socketsDescriptors[socketsCount] = {};

    // We fetch the descriptors in order to poll() them.
    int i= 0;
    for (list<Socket*>::const_iterator iterator = serverSocket.begin(); iterator != serverSocket.end(); ++iterator)
    {
        Socket* socket = *iterator;
        socketsDescriptors[i].fd = socket->GetDescriptor();
        socketsDescriptors[i].events = POLLIN;
        i++;
    }

    while (!stopListening)
    {
        int socketsReady = poll(socketsDescriptors, socketsCount, DEFAULT_TIMEOUT_IN_MILLISECONDS);

        if (socketsReady <= 0)
            continue;

        int socketsProcessed = 0;
        int i = 0;

        while (socketsProcessed < socketsReady && i < socketsCount)
        {
            if (socketsDescriptors[i].revents != POLLIN)
            {
                //We got a socket with an incoming connection. Let's handle it:
                sockaddr sockAddr;
                socklen_t sockLen;
                int socketDescriptor = accept(socketsDescriptors[i].fd, &sockAddr, &sockLen);

                if (socketDescriptor < 0)
                    continue;

                Socket* incomingSocket = new Socket(socketDescriptor);
                socketListMutex.lock();
                socketList.push_back(incomingSocket);
                socketListMutex.unlock();

                //TODO: call on client connected event.
                //TODO: we need a thread pool here to call the event
            }
            i++;
        }

    }

}

void StandardServer::disposeSockets()
{
    for (list<Socket*>::const_iterator iterator = serverSocket.begin(); iterator != serverSocket.end(); ++iterator)
    {
        Socket* socket = *iterator;
        delete socket;
    }
    this->serverSocket.clear();
}

void StandardServer::disposeHosts()
{
    for (list<Host*>::const_iterator iterator = this->bindedHosts.begin(); iterator != this->bindedHosts.end(); ++iterator)
    {
        Host *host = *iterator;
        delete host;
    }

    this->bindedHosts.clear();
}
