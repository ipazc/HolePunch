//
// Created by ivan on 16/09/15.
//

#ifndef HOLEPUNCHTEST_HOST_H
#define HOLEPUNCHTEST_HOST_H

#include <iostream>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>

using namespace std;

struct Host
{
private:
    string address;
    string hostname;

    int port;

    sockaddr_in networkSocketAddress;

    static in_addr translateAddress(string pAddress)
    {
        in_addr translatedAddr = {};

        addrinfo *res{};
        addrinfo hints{0, AF_INET, 0, 0, 0, nullptr, nullptr, nullptr};

        auto result = getaddrinfo(pAddress.c_str(), nullptr, &hints, &res);

        if (result == 0)
        {
            sockaddr_in *socketAddr = (sockaddr_in *) res->ai_addr;
            translatedAddr = socketAddr->sin_addr;
        }

        return translatedAddr;
    }

    static sockaddr_in generateNetworkSocketAddress(string pAddress, int pPort)
    {
        sockaddr_in socketAddress{};
        socketAddress.sin_addr = translateAddress(pAddress);
        socketAddress.sin_port = htons(pPort);
        socketAddress.sin_family = AF_INET;
        return socketAddress;
    }

public:
    Host(string pHostname, int pPort) : hostname(pHostname), port(pPort),
                                        networkSocketAddress(generateNetworkSocketAddress(pHostname, pPort))
    {
        char address[INET_ADDRSTRLEN];
        inet_ntop(AF_INET, &this->networkSocketAddress.sin_addr, address, INET_ADDRSTRLEN);
        this->address = address;
    };

    string GetAddress() { return this->address; };

    int GetPort() { return this->port; };

    sockaddr_in GetNetworkSocketAddress() { return this->networkSocketAddress; };
};

#endif //HOLEPUNCHTEST_HOST_H
