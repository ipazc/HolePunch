//
// Created by ivan on 17/09/15.
//

#ifndef HOLEPUNCHTEST_SOCKETEXCEPTION_H
#define HOLEPUNCHTEST_SOCKETEXCEPTION_H

#include <exception>
#include <iostream>

using namespace std;

class SocketException : public exception
{
protected:
    string message = "";

public:
    SocketException(string pMessage) : message(pMessage) { };

    virtual const char *what() const throw()
    {
        return this->message.c_str();
    }
};

#endif //HOLEPUNCHTEST_SOCKETEXCEPTION_H
